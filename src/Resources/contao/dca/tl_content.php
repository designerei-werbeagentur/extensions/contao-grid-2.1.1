<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

// Add subpalettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['__selector__'][] = 'gridSystem';
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['gridSystem_flexbox'] = 'flexGutter, flexDirection, flexWrap, flexClass';
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['gridSystem_grid'] = 'gridTemplateColumns, gridTemplateRows, gridGap, gridClass';

// Add fields
$GLOBALS['TL_DCA']['tl_content']['fields']['invisible']['eval']['tl_class'] = 'w50';

$GLOBALS['TL_DCA']['tl_content']['fields']['gridSystem'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                  'flexbox',
                                  'grid'
                                ),
    'eval'                    => array('tl_class'=>'w50', 'submitOnChange'=>true, 'includeBlankOption'=>true),
    'reference'               => &$GLOBALS['TL_LANG']['tl_content']['gridSystem'],
    'sql'                     => "varchar(32) default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['flexGutter'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                  'gutter-xs','gutter-sm','gutter-md','gutter-lg','gutter-xl',
                                  'sm:gutter-xs','sm:gutter-sm','sm:gutter-md','sm:gutter-lg','sm:gutter-xl',
                                  'md:gutter-xs','md:gutter-sm','md:gutter-md','md:gutter-lg','md:gutter-xl',
                                  'lg:gutter-xs','lg:gutter-sm','lg:gutter-md','lg:gutter-lg','lg:gutter-xl',
                                  'xl:gutter-xs','xl:gutter-sm','xl:gutter-md','xl:gutter-lg','xl:gutter-xl'
    ),
    'eval'                    => array('tl_class'=>'w50 clr', 'multiple'=>true, 'size'=>'10', 'chosen'=>true, 'mandatory'=>false),
    'sql'                     => "text NULL"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['flexDirection'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                  'flex-row','flex-row-reverse','flex-col','flex-col-reverse',
                                  'sm:flex-row','sm:flex-row-reverse','sm:flex-col','sm:flex-col-reverse',
                                  'md:flex-row','md:flex-row-reverse','md:flex-col','md:flex-col-reverse',
                                  'lg:flex-row','lg:flex-row-reverse','lg:flex-col','lg:flex-col-reverse',
                                  'xl:flex-row','xl:flex-row-reverse','xl:flex-col','xl:flex-col-reverse'
    ),
    'eval'                    => array('tl_class'=>'w50', 'multiple'=>true, 'size'=>'10', 'chosen'=>true, 'mandatory'=>false),
    'sql'                     => "text NULL"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['flexWrap'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                  'flex-wrap','flex-nowrap','flex-wrap-reverse',
                                  'sm:flex-wrap','sm:flex-nowrap','sm:flex-wrap-reverse',
                                  'md:flex-wrap','md:flex-nowrap','md:flex-wrap-reverse',
                                  'lg:flex-wrap','lg:flex-nowrap','lg:flex-wrap-reverse',
                                  'xl:flex-wrap','xl:flex-nowrap','xl:flex-wrap-reverse'
    ),
    'eval'                    => array('tl_class'=>'w50', 'multiple'=>true, 'size'=>'10', 'chosen'=>true, 'mandatory'=>false),
    'sql'                     => "text NULL"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['flexClass'] = array
(
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['gridGap'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                  'gap-0', 'gap-x-0', 'gap-y-0', 'sm:gap-0', 'sm:gap-x-0', 'sm:gap-y-0', 'md:gap-0', 'md:gap-x-0', 'md:gap-y-0', 'lg:gap-0', 'lg:gap-x-0', 'lg:gap-y-0', 'xl:gap-0', 'xl:gap-x-0', 'xl:gap-y-0',
                                  'gap-0.5', 'gap-x-0.5', 'gap-y-0.5', 'sm:gap-0.5', 'sm:gap-x-0.5', 'sm:gap-y-0.5', 'md:gap-0.5', 'md:gap-x-0.5', 'md:gap-y-0.5', 'lg:gap-0.5', 'lg:gap-x-0.5', 'lg:gap-y-0.5', 'xl:gap-0.5', 'xl:gap-x-0.5', 'xl:gap-y-0.5',
                                  'gap-1', 'gap-x-1', 'gap-y-1', 'sm:gap-1', 'sm:gap-x-1', 'sm:gap-y-1', 'md:gap-1', 'md:gap-x-1', 'md:gap-y-1', 'lg:gap-1', 'lg:gap-x-1', 'lg:gap-y-1', 'xl:gap-1', 'xl:gap-x-1', 'xl:gap-y-1',
                                  'gap-1.5', 'gap-x-1.5', 'gap-y-1.5', 'sm:gap-1.5', 'sm:gap-x-1.5', 'sm:gap-y-1.5', 'md:gap-1.5', 'md:gap-x-1.5', 'md:gap-y-1.5', 'lg:gap-1.5', 'lg:gap-x-1.5', 'lg:gap-y-1.5', 'xl:gap-1.5', 'xl:gap-x-1.5', 'xl:gap-y-1.5',
                                  'gap-2', 'gap-x-2', 'gap-y-2', 'sm:gap-2', 'sm:gap-x-2', 'sm:gap-y-2', 'md:gap-2', 'md:gap-x-2', 'md:gap-y-2', 'lg:gap-2', 'lg:gap-x-2', 'lg:gap-y-2', 'xl:gap-2', 'xl:gap-x-2', 'xl:gap-y-2',
                                  'gap-2.5', 'gap-x-2.5', 'gap-y-2.5', 'sm:gap-2.5', 'sm:gap-x-2.5', 'sm:gap-y-2.5', 'md:gap-2.5', 'md:gap-x-2.5', 'md:gap-y-2.5', 'lg:gap-2.5', 'lg:gap-x-2.5', 'lg:gap-y-2.5', 'xl:gap-2.5', 'xl:gap-x-2.5', 'xl:gap-y-2.5',
                                  'gap-3', 'gap-x-3', 'gap-y-3', 'sm:gap-3', 'sm:gap-x-3', 'sm:gap-y-3', 'md:gap-3', 'md:gap-x-3', 'md:gap-y-3', 'lg:gap-3', 'lg:gap-x-3', 'lg:gap-y-3', 'xl:gap-3', 'xl:gap-x-3', 'xl:gap-y-3',
                                  'gap-3.5', 'gap-x-3.5', 'gap-y-3.5', 'sm:gap-3.5', 'sm:gap-x-3.5', 'sm:gap-y-3.5', 'md:gap-3.5', 'md:gap-x-3.5', 'md:gap-y-3.5', 'lg:gap-3.5', 'lg:gap-x-3.5', 'lg:gap-y-3.5', 'xl:gap-3.5', 'xl:gap-x-3.5', 'xl:gap-y-3.5',
                                  'gap-4', 'gap-x-4', 'gap-y-4', 'sm:gap-4', 'sm:gap-x-4', 'sm:gap-y-4', 'md:gap-4', 'md:gap-x-4', 'md:gap-y-4', 'lg:gap-4', 'lg:gap-x-4', 'lg:gap-y-4', 'xl:gap-4', 'xl:gap-x-4', 'xl:gap-y-4',
                                  'gap-5', 'gap-x-5', 'gap-y-5', 'sm:gap-5', 'sm:gap-x-5', 'sm:gap-y-5', 'md:gap-5', 'md:gap-x-5', 'md:gap-y-5', 'lg:gap-5', 'lg:gap-x-5', 'lg:gap-y-5', 'xl:gap-5', 'xl:gap-x-5', 'xl:gap-y-5',
                                  'gap-6', 'gap-x-6', 'gap-y-6', 'sm:gap-6', 'sm:gap-x-6', 'sm:gap-y-6', 'md:gap-6', 'md:gap-x-6', 'md:gap-y-6', 'lg:gap-6', 'lg:gap-x-6', 'lg:gap-y-6', 'xl:gap-6', 'xl:gap-x-6', 'xl:gap-y-6',
                                  'gap-7', 'gap-x-7', 'gap-y-7', 'sm:gap-7', 'sm:gap-x-7', 'sm:gap-y-7', 'md:gap-7', 'md:gap-x-7', 'md:gap-y-7', 'lg:gap-7', 'lg:gap-x-7', 'lg:gap-y-7', 'xl:gap-7', 'xl:gap-x-7', 'xl:gap-y-7',
                                  'gap-8', 'gap-x-8', 'gap-y-8', 'sm:gap-8', 'sm:gap-x-8', 'sm:gap-y-8', 'md:gap-8', 'md:gap-x-8', 'md:gap-y-8', 'lg:gap-8', 'lg:gap-x-8', 'lg:gap-y-8', 'xl:gap-8', 'xl:gap-x-8', 'xl:gap-y-8',
                                  'gap-9', 'gap-x-9', 'gap-y-9', 'sm:gap-9', 'sm:gap-x-9', 'sm:gap-y-9', 'md:gap-9', 'md:gap-x-9', 'md:gap-y-9', 'lg:gap-9', 'lg:gap-x-9', 'lg:gap-y-9', 'xl:gap-9', 'xl:gap-x-9', 'xl:gap-y-9',
                                  'gap-10', 'gap-x-10', 'gap-y-10', 'sm:gap-10', 'sm:gap-x-10', 'sm:gap-y-10', 'md:gap-10', 'md:gap-x-10', 'md:gap-y-10', 'lg:gap-10', 'lg:gap-x-10', 'lg:gap-y-10', 'xl:gap-10', 'xl:gap-x-10', 'xl:gap-y-10',
                                  'gap-11', 'gap-x-11', 'gap-y-11', 'sm:gap-11', 'sm:gap-x-11', 'sm:gap-y-11', 'md:gap-11', 'md:gap-x-11', 'md:gap-y-11', 'lg:gap-11', 'lg:gap-x-11', 'lg:gap-y-11', 'xl:gap-11', 'xl:gap-x-11', 'xl:gap-y-11',
                                  'gap-12', 'gap-x-12', 'gap-y-12', 'sm:gap-12', 'sm:gap-x-12', 'sm:gap-y-12', 'md:gap-12', 'md:gap-x-12', 'md:gap-y-12', 'lg:gap-12', 'lg:gap-x-12', 'lg:gap-y-12', 'xl:gap-12', 'xl:gap-x-12', 'xl:gap-y-12',
                                  'gap-14', 'gap-x-14', 'gap-y-14', 'sm:gap-14', 'sm:gap-x-14', 'sm:gap-y-14', 'md:gap-14', 'md:gap-x-14', 'md:gap-y-14', 'lg:gap-14', 'lg:gap-x-14', 'lg:gap-y-14', 'xl:gap-14', 'xl:gap-x-14', 'xl:gap-y-14',
                                  'gap-16', 'gap-x-16', 'gap-y-16', 'sm:gap-16', 'sm:gap-x-16', 'sm:gap-y-16', 'md:gap-16', 'md:gap-x-16', 'md:gap-y-16', 'lg:gap-16', 'lg:gap-x-16', 'lg:gap-y-16', 'xl:gap-16', 'xl:gap-x-16', 'xl:gap-y-16',
                                  'gap-20', 'gap-x-20', 'gap-y-20', 'sm:gap-20', 'sm:gap-x-20', 'sm:gap-y-20', 'md:gap-20', 'md:gap-x-20', 'md:gap-y-20', 'lg:gap-20', 'lg:gap-x-20', 'lg:gap-y-20', 'xl:gap-20', 'xl:gap-x-20', 'xl:gap-y-20',
                                  'gap-24', 'gap-x-24', 'gap-y-24', 'sm:gap-24', 'sm:gap-x-24', 'sm:gap-y-24', 'md:gap-24', 'md:gap-x-24', 'md:gap-y-24', 'lg:gap-24', 'lg:gap-x-24', 'lg:gap-y-24', 'xl:gap-24', 'xl:gap-x-24', 'xl:gap-y-24',
                                  'gap-28', 'gap-x-28', 'gap-y-28', 'sm:gap-28', 'sm:gap-x-28', 'sm:gap-y-28', 'md:gap-28', 'md:gap-x-28', 'md:gap-y-28', 'lg:gap-28', 'lg:gap-x-28', 'lg:gap-y-28', 'xl:gap-28', 'xl:gap-x-28', 'xl:gap-y-28',
                                  'gap-32', 'gap-x-32', 'gap-y-32', 'sm:gap-32', 'sm:gap-x-32', 'sm:gap-y-32', 'md:gap-32', 'md:gap-x-32', 'md:gap-y-32', 'lg:gap-32', 'lg:gap-x-32', 'lg:gap-y-32', 'xl:gap-32', 'xl:gap-x-32', 'xl:gap-y-32',
                                  'gap-36', 'gap-x-36', 'gap-y-36', 'sm:gap-36', 'sm:gap-x-36', 'sm:gap-y-36', 'md:gap-36', 'md:gap-x-36', 'md:gap-y-36', 'lg:gap-36', 'lg:gap-x-36', 'lg:gap-y-36', 'xl:gap-36', 'xl:gap-x-36', 'xl:gap-y-36',
                                  'gap-40', 'gap-x-40', 'gap-y-40', 'sm:gap-40', 'sm:gap-x-40', 'sm:gap-y-40', 'md:gap-40', 'md:gap-x-40', 'md:gap-y-40', 'lg:gap-40', 'lg:gap-x-40', 'lg:gap-y-40', 'xl:gap-40', 'xl:gap-x-40', 'xl:gap-y-40',
                                  'gap-44', 'gap-x-44', 'gap-y-44', 'sm:gap-44', 'sm:gap-x-44', 'sm:gap-y-44', 'md:gap-44', 'md:gap-x-44', 'md:gap-y-44', 'lg:gap-44', 'lg:gap-x-44', 'lg:gap-y-44', 'xl:gap-44', 'xl:gap-x-44', 'xl:gap-y-44',
                                  'gap-52', 'gap-x-52', 'gap-y-52', 'sm:gap-52', 'sm:gap-x-52', 'sm:gap-y-52', 'md:gap-52', 'md:gap-x-52', 'md:gap-y-52', 'lg:gap-52', 'lg:gap-x-52', 'lg:gap-y-52', 'xl:gap-52', 'xl:gap-x-52', 'xl:gap-y-52',
                                  'gap-56', 'gap-x-56', 'gap-y-56', 'sm:gap-56', 'sm:gap-x-56', 'sm:gap-y-56', 'md:gap-56', 'md:gap-x-56', 'md:gap-y-56', 'lg:gap-56', 'lg:gap-x-56', 'lg:gap-y-56', 'xl:gap-56', 'xl:gap-x-56', 'xl:gap-y-56',
                                  'gap-60', 'gap-x-60', 'gap-y-60', 'sm:gap-60', 'sm:gap-x-60', 'sm:gap-y-60', 'md:gap-60', 'md:gap-x-60', 'md:gap-y-60', 'lg:gap-60', 'lg:gap-x-60', 'lg:gap-y-60', 'xl:gap-60', 'xl:gap-x-60', 'xl:gap-y-60',
                                  'gap-64', 'gap-x-64', 'gap-y-64', 'sm:gap-64', 'sm:gap-x-64', 'sm:gap-y-64', 'md:gap-64', 'md:gap-x-64', 'md:gap-y-64', 'lg:gap-64', 'lg:gap-x-64', 'lg:gap-y-64', 'xl:gap-64', 'xl:gap-x-64', 'xl:gap-y-64',
                                  'gap-72', 'gap-x-72', 'gap-y-72', 'sm:gap-72', 'sm:gap-x-72', 'sm:gap-y-72', 'md:gap-72', 'md:gap-x-72', 'md:gap-y-72', 'lg:gap-72', 'lg:gap-x-72', 'lg:gap-y-72', 'xl:gap-72', 'xl:gap-x-72', 'xl:gap-y-72',
                                  'gap-80', 'gap-x-80', 'gap-y-80', 'sm:gap-80', 'sm:gap-x-80', 'sm:gap-y-80', 'md:gap-80', 'md:gap-x-80', 'md:gap-y-80', 'lg:gap-80', 'lg:gap-x-80', 'lg:gap-y-80', 'xl:gap-80', 'xl:gap-x-80', 'xl:gap-y-80',
                                  'gap-96', 'gap-x-96', 'gap-y-96', 'sm:gap-96', 'sm:gap-x-96', 'sm:gap-y-96', 'md:gap-96', 'md:gap-x-96', 'md:gap-y-96', 'lg:gap-96', 'lg:gap-x-96', 'lg:gap-y-96', 'xl:gap-96', 'xl:gap-x-96', 'xl:gap-y-96',
                                  'gap-px', 'gap-x-px', 'gap-y-px', 'sm:gap-px', 'sm:gap-x-px', 'sm:gap-y-px', 'md:gap-px', 'md:gap-x-px', 'md:gap-y-px', 'lg:gap-px', 'lg:gap-x-px', 'lg:gap-y-px', 'xl:gap-px', 'xl:gap-x-px', 'xl:gap-y-px'
    ),
    'eval'                    => array('tl_class'=>'w50', 'multiple'=>true, 'size'=>'10', 'chosen'=>true, 'mandatory'=>false),
    'sql'                     => "text NULL"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['gridTemplateColumns'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                  'grid-cols-1', 'grid-cols-2', 'grid-cols-3', 'grid-cols-4', 'grid-cols-5', 'grid-cols-6', 'grid-cols-7', 'grid-cols-8', 'grid-cols-9', 'grid-cols-10', 'grid-cols-11', 'grid-cols-12', 'grid-cols-none',
                                  'sm:grid-cols-1', 'sm:grid-cols-2', 'sm:grid-cols-3', 'sm:grid-cols-4', 'sm:grid-cols-5', 'sm:grid-cols-6', 'sm:grid-cols-7', 'sm:grid-cols-8', 'sm:grid-cols-9', 'sm:grid-cols-10', 'sm:grid-cols-11', 'sm:grid-cols-12', 'sm:grid-cols-none',
                                  'md:grid-cols-1', 'md:grid-cols-2', 'md:grid-cols-3', 'md:grid-cols-4', 'md:grid-cols-5', 'md:grid-cols-6', 'md:grid-cols-7', 'md:grid-cols-8', 'md:grid-cols-9', 'md:grid-cols-10', 'md:grid-cols-11', 'md:grid-cols-12', 'md:grid-cols-none',
                                  'lg:grid-cols-1', 'lg:grid-cols-2', 'lg:grid-cols-3', 'lg:grid-cols-4', 'lg:grid-cols-5', 'lg:grid-cols-6', 'lg:grid-cols-7', 'lg:grid-cols-8', 'lg:grid-cols-9', 'lg:grid-cols-10', 'lg:grid-cols-11', 'lg:grid-cols-12', 'lg:grid-cols-none',
                                  'xl:grid-cols-1', 'xl:grid-cols-2', 'xl:grid-cols-3', 'xl:grid-cols-4', 'xl:grid-cols-5', 'xl:grid-cols-6', 'xl:grid-cols-7', 'xl:grid-cols-8', 'xl:grid-cols-9', 'xl:grid-cols-10', 'xl:grid-cols-11', 'xl:grid-cols-12', 'xl:grid-cols-none'
    ),
    'eval'                    => array('tl_class'=>'w50', 'multiple'=>true, 'size'=>'10', 'chosen'=>true, 'mandatory'=>false),
    'sql'                     => "text NULL"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['gridTemplateRows'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                  'grid-rows-1', 'grid-rows-2', 'grid-rows-3', 'grid-rows-4', 'grid-rows-5', 'grid-rows-6', 'grid-rows-none',
                                  'sm:grid-rows-1', 'sm:grid-rows-2', 'sm:grid-rows-3', 'sm:grid-rows-4', 'sm:grid-rows-5', 'sm:grid-rows-6', 'sm:grid-rows-none',
                                  'md:grid-rows-1', 'md:grid-rows-2', 'md:grid-rows-3', 'md:grid-rows-4', 'md:grid-rows-5', 'md:grid-rows-6', 'md:grid-rows-none',
                                  'lg:grid-rows-1', 'lg:grid-rows-2', 'lg:grid-rows-3', 'lg:grid-rows-4', 'lg:grid-rows-5', 'lg:grid-rows-6', 'lg:grid-rows-none',
                                  'xl:grid-rows-1', 'xl:grid-rows-2', 'xl:grid-rows-3', 'xl:grid-rows-4', 'xl:grid-rows-5', 'xl:grid-rows-6', 'xl:grid-rows-none',
    ),
    'eval'                    => array('tl_class'=>'w50', 'multiple'=>true, 'size'=>'10', 'chosen'=>true, 'mandatory'=>false),
    'sql'                     => "text NULL"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['gridClass'] = array
(
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['containerWrapper'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                  'max-w-0', 'max-w-none', 'max-w-xs', 'max-w-sm', 'max-w-md', 'max-w-xl', 'max-w-2xl', 'max-w-3xl', 'max-w-4xl', 'max-w-5xl', 'max-w-6xl', 'max-w-7xl', 'max-w-full', 'max-w-min', 'max-w-max', 'max-w-prose', 'max-w-screen-sm', 'max-w-screen-md', 'max-w-screen-lg', 'max-w-screen-xl',
                                  'sm:max-w-0', 'sm:max-w-none', 'sm:max-w-xs', 'sm:max-w-sm', 'sm:max-w-md', 'sm:max-w-xl', 'sm:max-w-2xl', 'sm:max-w-3xl', 'sm:max-w-4xl', 'sm:max-w-5xl', 'sm:max-w-6xl', 'sm:max-w-7xl', 'sm:max-w-full', 'sm:max-w-min', 'sm:max-w-max', 'sm:max-w-prose', 'sm:max-w-screen-sm', 'sm:max-w-screen-md', 'sm:max-w-screen-lg', 'sm:max-w-screen-xl',
                                  'md:max-w-0', 'md:max-w-none', 'md:max-w-xs', 'md:max-w-sm', 'md:max-w-md', 'md:max-w-xl', 'md:max-w-2xl', 'md:max-w-3xl', 'md:max-w-4xl', 'md:max-w-5xl', 'md:max-w-6xl', 'md:max-w-7xl', 'md:max-w-full', 'md:max-w-min', 'md:max-w-max', 'md:max-w-prose', 'md:max-w-screen-sm', 'md:max-w-screen-md', 'md:max-w-screen-lg', 'md:max-w-screen-xl',
                                  'lg:max-w-0', 'lg:max-w-none', 'lg:max-w-xs', 'lg:max-w-sm', 'lg:max-w-md', 'lg:max-w-xl', 'lg:max-w-2xl', 'lg:max-w-3xl', 'lg:max-w-4xl', 'lg:max-w-5xl', 'lg:max-w-6xl', 'lg:max-w-7xl', 'lg:max-w-full', 'lg:max-w-min', 'lg:max-w-max', 'lg:max-w-prose', 'lg:max-w-screen-sm', 'lg:max-w-screen-md', 'lg:max-w-screen-lg', 'lg:max-w-screen-xl',
                                  'xl:max-w-0', 'xl:max-w-none', 'xl:max-w-xs', 'xl:max-w-sm', 'xl:max-w-md', 'xl:max-w-xl', 'xl:max-w-2xl', 'xl:max-w-3xl', 'xl:max-w-4xl', 'xl:max-w-5xl', 'xl:max-w-6xl', 'xl:max-w-7xl', 'xl:max-w-full', 'xl:max-w-min', 'xl:max-w-max', 'xl:max-w-prose', 'xl:max-w-screen-sm', 'xl:max-w-screen-md', 'xl:max-w-screen-lg', 'xl:max-w-screen-xl'
                              ),
    'eval'                    => array('tl_class'=>'w50', 'multiple'=>true, 'size'=>'10', 'chosen'=>true, 'mandatory'=>false),
    'sql'                     => "text NULL"
);

PaletteManipulator::create()
  ->removeField('sh5_type', 'html5_legend')
  ->removeField('sh5_additional', 'html5_legend')
  ->addField('gridSystem', 'html5_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('containerWrapper', 'expert_legend', PaletteManipulator::POSITION_APPEND)
  ->applyToPalette('sHtml5Start', 'tl_content')
;
