<?php

namespace designerei\ContaoGridBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;
use Terminal42\ServiceAnnotationBundle\ServiceAnnotationInterface;

class ContaoGridListener implements ServiceAnnotationInterface
{
    /**
     * @Hook("parseTemplate")
     */
    public function onParseTemplate(Template $template): void
    {
        if ('ce_semantic_html5_start' === $template->getName()) {

            // remove default classes
            $template->class = str_replace('ce_sHtml5Start', '', $template->class);

            // add grid or container class
            if ($template->gridSystem == 'flexbox') {
                $template->class = 'ce_grid' . $template->class;
                $template->insideClass = 'flex' . $template->insideClass;
            } elseif ($template->gridSystem == 'grid') {
                $template->class = 'ce_grid' . $template->class;
                $template->insideClass = 'grid' . $template->insideClass;
            } else {
                $template->class = 'ce_container' . $template->class;
            }

            // add containerWrapper class
            if($template->containerWrapper) {

                // add classes
                $template->containerWrapper = deserialize($template->containerWrapper);
                foreach($template->containerWrapper as $value) {
                  $template->class .= ' ' . $value;
                }

                // add mx-auto
                $template->class .= ' mx-auto';
            }

            // add inside classes
            if ($template->gridSystem == 'flexbox' || $template->gridSystem == 'grid') {

              if ($template->gridSystem == 'flexbox') {
                $selectFields = array(
                  'flexGutter', 'flexDirection', 'flexWrap',
                );
              } elseif ($template->gridSystem == 'grid') {
                $selectFields = array(
                  'gridTemplateColumns', 'gridTemplateRows', 'gridGap'
                );
              }

              foreach ($selectFields as $field) {
                  if ($template->$field) {
                      $template->$field = deserialize($template->$field);
                      foreach($template->$field as $value) {
                        $template->insideClass .= ' ' . $value;
                      }
                  }
              }
            }

            // add flexClass / gridClass to inside classes
            if ($template->gridSystem == 'flexbox') {
              if ($template->flexClass) {
                $template->insideClass .= ' ' . $template->flexClass;
              }
            } elseif ($template->gridSystem == 'grid') {
              if ($template->gridClass) {
                $template->insideClass .= ' ' . $template->gridClass;
              }
            }
        }
    }
}
